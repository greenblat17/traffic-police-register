package com.greenblat.trafficregister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrafficRegisterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrafficRegisterApplication.class, args);
    }

}
